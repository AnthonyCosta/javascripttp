function getTitle(){
	let title = document.querySelectorAll('h2');
	title.forEach (function (titre){
			logMessage(titre.innerText);
	});
}

function remplace(){
	document.querySelector("button").addEventListener('click', clicker);
}

function details(){
	let detail = document.querySelectorAll("a");
	for(let i=0; i < detail.length; i++){
   		detail[i].addEventListener('click', function(clicker2){
   			let description = document.getElementsByClassName("text-news");

	 		if(i === 0)
	 			logMessage(description[0].innerText);
	 		else if(i == 1)
	 			logMessage(description[1].innerText);
	 		else
	 			logMessage(description[2].innerText);
	 	}, false);
   	}
}

function clicker(click){
	let trouver = 0;
	let input = document.querySelector("input").value;
	let h1 = document.querySelector('h1');
	let title = document.querySelectorAll('h2');
	let para = document.querySelectorAll("p");
	let message = document.getElementById('error-message');

	click.preventDefault();

	document.querySelector('h1').innerText = input;

	title.forEach (function (titre){
			if(titre.innerText.includes(input)){
				titre.style.color = blue;
				trouver = 1;
			}
			else
				titre.style.color = "black";
	});

	para.forEach (function (description){
			if(description.innerText.includes(input)){
				description.style.color = blue;
				trouver = 1;
			}
			else
				description.style.color = "black";
	});

	if(trouver === 0)
		h1.style.color = red;
	else
		h1.style.color = "black";
	
	if(input === ""){
        message.innerText = 'Saisie vide';
		message.style.color = 'white';
		message.style.marginLeft = '50px';
		message.style.visibility = 'visible';
    }
    else{
    	message.style.visibility = 'hidden';
    }
}